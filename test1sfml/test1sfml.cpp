﻿#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;

int main()
{
    RenderWindow window(VideoMode(1280, 860), "h3log");

    bool ScrollTypePressed = false;
    bool ScrollMessagePressed = false;

    Texture backgTexture;
    backgTexture.loadFromFile("maket.jpg");

    Sprite backgSprite;
    backgSprite.setTexture(backgTexture);
    backgSprite.setPosition(0, 0);


    RectangleShape button1(Vector2f(303, 56)); // 
    button1.setFillColor(Color(0, 0, 0, 0));
    button1.setPosition(366, 125);

    RectangleShape button2(Vector2f(546, 56));
    button2.setFillColor(Color(255, 255, 255, 0));
    button2.setPosition(674, 125);

    RectangleShape ScrollType(Vector2f(60, 29)); //скрол type
    ScrollType.setFillColor(Color(0, 0, 0));
    ScrollType.setPosition(371, 718);

    RectangleShape ScrollMessage(Vector2f(120, 29)); //скрол message
    ScrollMessage.setFillColor(Color(0, 0, 0));
    ScrollMessage.setPosition(680, 718);

    RectangleShape InputFromHorus(Vector2f(174, 63)); //от часы
    InputFromHorus.setPosition(62, 52);

    RectangleShape InputFromMinutes(Vector2f(174, 63)); //от минут
    InputFromMinutes.setPosition(248, 52);

    RectangleShape InputFromSeconds(Vector2f(174, 63)); //от секунд
    InputFromSeconds.setPosition(435, 52);

    RectangleShape InputToHorus(Vector2f(174, 63));  //до часов
    InputToHorus.setPosition(679, 52);

    RectangleShape InputToMinutes(Vector2f(174, 63));  //до минут
    InputToMinutes.setPosition(863, 52);

    RectangleShape InputToSeconds(Vector2f(174, 63));  //до секунд
    InputToSeconds.setPosition(1045, 52);

    RectangleShape SearchBar(Vector2f(570, 42));  
    SearchBar.setPosition(490, 785);
    


    int dXscrollType = ScrollType.getPosition().x;
    int dXscrollMessage = ScrollMessage.getPosition().x;

    int OlMousePositionScrollType;
    bool SetPositionScrollType = false;

    int OlMousePositionScrollMessage;
    bool SetPositionScrollMessage = false;

    int DOUBLE_input = 0;
    bool AvailableInputFromHours = false;
    bool AvailableInputFromMinutes = false;
    bool  AvailableInputFromSeconds = false;
    bool AvailableInputToHours = false;
    bool  AvailableInputToMinutes = false;
    bool AvailableInputToSeconds = false;

    bool AvailableInputSearchBar = false;

    Font font;
    font.loadFromFile("Heroes2HQ.ttf");

    Font SearchFont;
    SearchFont.loadFromFile("times-new-roman.ttf");

    std::string FromHour;
    Text FromHoursText("", font, 50);
    FromHoursText.setPosition(125, 55);

    std::string FromMinutes;
    Text FromMinutesText("", font, 50);
    FromMinutesText.setPosition(305, 55);

    std::string FromSeconds;
    Text FromSecondsText("", font, 50);
    FromSecondsText.setPosition(510, 55);

    std::string ToHour;
    Text ToHoursText("", font, 50);
    ToHoursText.setPosition(740, 55);

    std::string ToMinutes;
    Text ToMinutesText("", font, 50);
    ToMinutesText.setPosition(930, 55);

    std::string ToSeconds;
    Text ToSecondsText("", font, 50);
    ToSecondsText.setPosition(1120, 55);

    std::string Search;
    Text SearchText("", SearchFont, 40);
    SearchText.setPosition(525 , 780);

    std::string InputHelper;
    Text InputHelperText(">", font, 50); 
    InputHelperText.setFillColor(Color(255, 255, 255, 0));

    std::string InputHelperSearchBar;
    Text InputHelperSearchText(">", font, 50);
    InputHelperSearchText.setFillColor(Color(255, 255, 255, 0));


    while (window.isOpen())
    {
        Vector2i MousePosition = Mouse::getPosition(window);

        Event event;
        while (window.pollEvent(event))
        {
            if (event.type == Event::Closed)
                window.close();

            //записывание числа в ячейку от часы
            if (event.type == Event::TextEntered && AvailableInputFromHours==true)  
            {
                if (DOUBLE_input < 2) {
                    // отсекаем не ASCII символы
                    if (event.text.unicode < 128)
                    {
                        FromHour += static_cast<char>(event.text.unicode);
                        FromHoursText.setString(FromHour);
                        std::cout << FromHour;
                        DOUBLE_input++;  
                        if(DOUBLE_input==2)
                        {
                            FromMinutesText.setFillColor(Color(255, 255, 255));
                            InputHelperText.setPosition(285, 46);
                            FromHoursText.setFillColor(Color(118, 134, 167));
                        }
                    }
                }
                else {

                    FromMinutes = "";
                    FromMinutesText.setString(FromMinutes);

                    DOUBLE_input = 0;
                    AvailableInputFromHours = false;
                    AvailableInputFromMinutes = true;
                }
            } 

            //записывание числа в ячейку от минуты
            if (event.type == Event::TextEntered && AvailableInputFromMinutes == true)    
            {
                if (DOUBLE_input < 2) {
                    // отсекаем не ASCII символы
                    if (event.text.unicode < 128)
                    {
                        FromMinutes += static_cast<char>(event.text.unicode);
                        FromMinutesText.setString(FromMinutes);
                        std::cout << FromMinutes;
                        DOUBLE_input++;
                        if (DOUBLE_input == 2)
                        {
                            FromSecondsText.setFillColor(Color(255, 255, 255));
                            InputHelperText.setPosition(490, 46);
                            FromMinutesText.setFillColor(Color(118, 134, 167));
                        }
                    }
                }
                else {

                    FromSeconds = "";
                    FromSecondsText.setString(FromSeconds);

                    DOUBLE_input = 0;
                    AvailableInputFromMinutes = false;
                    AvailableInputFromSeconds = true;
                }
            }

            //записывание числа в ячейку от секунды
            if (event.type == Event::TextEntered && AvailableInputFromSeconds == true)   
            {
                if (DOUBLE_input < 2) {
                    // отсекаем не ASCII символы
                    if (event.text.unicode < 128)
                    {

                        FromSeconds += static_cast<char>(event.text.unicode);
                        FromSecondsText.setString(FromSeconds);
                        std::cout << FromSeconds;
                        DOUBLE_input++;
                        if (DOUBLE_input == 2)
                        {
                            ToHoursText.setFillColor(Color(255, 255, 255));
                            InputHelperText.setPosition(720, 46);
                            FromSecondsText.setFillColor(Color(118, 134, 167));
                        }
                    }
                }
                else {

                    ToHour = "";
                    ToHoursText.setString(ToHour);

                    DOUBLE_input = 0;
                    AvailableInputFromSeconds = false;
                    AvailableInputToHours = true;
                }
            }

            //записывание числа в ячейку до часы
            if (event.type == Event::TextEntered && AvailableInputToHours == true)  
            {
                if (DOUBLE_input < 2) {
                    // отсекаем не ASCII символы
                    if (event.text.unicode < 128)
                    {
                        ToHour += static_cast<char>(event.text.unicode);
                        ToHoursText.setString(ToHour);
                        std::cout << ToHour;
                        DOUBLE_input++;
                        if (DOUBLE_input == 2)
                        {
                            ToMinutesText.setFillColor(Color(255, 255, 255));
                            InputHelperText.setPosition(910, 46);
                            ToHoursText.setFillColor(Color(118, 134, 167));
                        }
                    }
                }
                else {

                    ToMinutes = "";
                    ToMinutesText.setString(FromMinutes);

                    DOUBLE_input = 0;
                    AvailableInputToHours = false;
                    AvailableInputToMinutes = true;
                }
            }

            //записывание числа в ячейку до минуты
            if (event.type == Event::TextEntered && AvailableInputToMinutes == true)
            {
                if (DOUBLE_input < 2) {
                    // отсекаем не ASCII символы
                    if (event.text.unicode < 128)
                    {
                        ToMinutes += static_cast<char>(event.text.unicode);
                        ToMinutesText.setString(ToMinutes);
                        std::cout << ToMinutes;
                        DOUBLE_input++;
                        if (DOUBLE_input == 2)
                        {
                            ToSecondsText.setFillColor(Color(255, 255, 255));
                            InputHelperText.setPosition(1100, 46);
                            ToMinutesText.setFillColor(Color(118, 134, 167));
                        }
                    }
                }
                else {

                    ToSeconds = "";
                    ToSecondsText.setString(FromSeconds);

                    DOUBLE_input = 0;
                    AvailableInputToMinutes = false;
                    AvailableInputToSeconds = true;
                }
            }

            //записывание числа в ячейку до секунды
            if (event.type == Event::TextEntered && AvailableInputToSeconds == true)
            {
                if (DOUBLE_input < 2) {
                    // отсекаем не ASCII символы
                    if (event.text.unicode < 128)
                    {

                        ToSeconds += static_cast<char>(event.text.unicode);
                        ToSecondsText.setString(ToSeconds);
                        std::cout << ToSeconds;
                        DOUBLE_input++;
                        if (DOUBLE_input == 2)
                        {                         
                            InputHelperText.setFillColor(Color(255, 255, 255, 0));
                            ToSecondsText.setFillColor(Color(118, 134, 167));
                        }
                    }
                }
                else {                    
                    DOUBLE_input = 0;
                    AvailableInputToSeconds = false;
                }
            }


                //поиск
            if (event.type == Event::TextEntered && AvailableInputSearchBar == true)
            {

                // отсекаем не ASCII символы
                if (event.text.unicode < 128)
                {

                    Search += static_cast<char>(event.text.unicode);
                    SearchText.setString(Search);
                    std::cout << Search;
                    
                }  

                if (Keyboard::isKeyPressed(Keyboard::Enter)) {
                    AvailableInputSearchBar = false;
                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));
                }

                if (Keyboard::isKeyPressed(Keyboard::BackSpace)) {
                    if (Search.length() != 1) {
                        Search.resize(Search.length() - 2);
                        SearchText.setString(Search);
                    }
                }
            }
        }

        // от часов
        if (event.type == Event::MouseButtonPressed ) 
            if (event.key.code == Mouse::Left)
                if (InputFromHorus.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperText.setFillColor(Color(255 , 255, 255, 255));
                    InputHelperText.setPosition(105, 46);

                    FromHour = "";
                    FromHoursText.setString(FromHour);
                    FromHoursText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = false;

                    AvailableInputFromHours = true;
                    AvailableInputFromMinutes = false;
                    AvailableInputFromSeconds = false;

                    AvailableInputToHours = false;
                    AvailableInputToMinutes = false;
                    AvailableInputToSeconds = false;

                    FromMinutesText.setFillColor(Color(118, 134, 167));
                    FromSecondsText.setFillColor(Color(118, 134, 167));

                    ToHoursText.setFillColor(Color(118, 134, 167));
                    ToMinutesText.setFillColor(Color(118, 134, 167));
                    ToSecondsText.setFillColor(Color(118, 134, 167));

                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));

                    DOUBLE_input = 0;

                }

        // от минут
        if (event.type == Event::MouseButtonPressed)   
            if (event.key.code == Mouse::Left)
                if (InputFromMinutes.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperText.setFillColor(Color(255, 255, 255, 255));
                    InputHelperText.setPosition(285, 46);

                    FromMinutes = "";
                    FromMinutesText.setString(FromMinutes);
                    FromMinutesText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = false;

                    AvailableInputFromMinutes = true;
                    AvailableInputFromHours = false;
                    AvailableInputFromSeconds = false;

                    AvailableInputToHours = false;
                    AvailableInputToMinutes = false;
                    AvailableInputToSeconds = false;

                    FromHoursText.setFillColor(Color(118, 134, 167));
                    FromSecondsText.setFillColor(Color(118, 134, 167));

                    ToHoursText.setFillColor(Color(118, 134, 167));
                    ToMinutesText.setFillColor(Color(118, 134, 167));
                    ToSecondsText.setFillColor(Color(118, 134, 167));

                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));

                    DOUBLE_input = 0;

                }

        // от секунд
        if (event.type == Event::MouseButtonPressed)   
            if (event.key.code == Mouse::Left)
                if (InputFromSeconds.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperText.setFillColor(Color(255, 255, 255, 255));
                    InputHelperText.setPosition(490, 46);

                    FromSeconds = "";
                    FromSecondsText.setString(FromSeconds);
                    FromSecondsText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = false;

                    AvailableInputFromSeconds = true;
                    AvailableInputFromHours = false;
                    AvailableInputFromMinutes = false;
                    
                    AvailableInputToHours = false;
                    AvailableInputToMinutes = false;
                    AvailableInputToSeconds = false;\

                    FromHoursText.setFillColor(Color(118, 134, 167));
                    FromMinutesText.setFillColor(Color(118, 134, 167));

                    ToHoursText.setFillColor(Color(118, 134, 167));
                    ToMinutesText.setFillColor(Color(118, 134, 167));
                    ToSecondsText.setFillColor(Color(118, 134, 167));

                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));

                    DOUBLE_input = 0;

                }

        // до часов
        if (event.type == Event::MouseButtonPressed) 
            if (event.key.code == Mouse::Left)
                if (InputToHorus.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperText.setFillColor(Color(255, 255, 255, 255));
                    InputHelperText.setPosition(720, 46);

                    ToHour = "";
                    ToHoursText.setString(ToHour);
                    ToHoursText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = false;

                    AvailableInputToHours = true;
                    AvailableInputToMinutes = false;
                    AvailableInputToSeconds = false;

                    AvailableInputFromHours = false;
                    AvailableInputFromMinutes = false;
                    AvailableInputFromSeconds = false;

                    FromHoursText.setFillColor(Color(118, 134, 167));
                    FromMinutesText.setFillColor(Color(118, 134, 167));
                    FromSecondsText.setFillColor(Color(118, 134, 167));

                    ToMinutesText.setFillColor(Color(118, 134, 167));
                    ToSecondsText.setFillColor(Color(118, 134, 167));

                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));

                    DOUBLE_input = 0;

                }

        // до минут
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (InputToMinutes.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperText.setFillColor(Color(255, 255, 255, 255));
                    InputHelperText.setPosition(910, 46);

                    ToMinutes = "";
                    ToMinutesText.setString(ToMinutes);
                    ToMinutesText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = false;

                    AvailableInputToMinutes = true;
                    AvailableInputToHours = false;
                    AvailableInputToSeconds = false;

                    AvailableInputFromHours = false;
                    AvailableInputFromMinutes = false;
                    AvailableInputFromSeconds = false;

                    FromHoursText.setFillColor(Color(118, 134, 167));
                    FromMinutesText.setFillColor(Color(118, 134, 167));
                    FromSecondsText.setFillColor(Color(118, 134, 167));

                    ToHoursText.setFillColor(Color(118, 134, 167));
                    ToSecondsText.setFillColor(Color(118, 134, 167));

                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));

                    DOUBLE_input = 0;

                }


        //до секунд
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (InputToSeconds.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperText.setFillColor(Color(255, 255, 255, 255));
                    InputHelperText.setPosition(1100, 46);

                    ToSeconds = "";
                    ToSecondsText.setString(ToSeconds);
                    ToSecondsText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = false;

                    AvailableInputToSeconds = true;
                    AvailableInputToHours = false;
                    AvailableInputToMinutes = false;
                    
                    AvailableInputFromHours = false;
                    AvailableInputFromMinutes = false;
                    AvailableInputFromSeconds = false;

                    FromHoursText.setFillColor(Color(118, 134, 167));
                    FromMinutesText.setFillColor(Color(118, 134, 167));
                    FromSecondsText.setFillColor(Color(118, 134, 167));

                    ToHoursText.setFillColor(Color(118, 134, 167));
                    ToMinutesText.setFillColor(Color(118, 134, 167));

                    SearchText.setFillColor(Color(118, 134, 167));
                    InputHelperSearchText.setFillColor(Color(118, 134, 167));

                    DOUBLE_input = 0;

                }


        // поиск
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (SearchBar.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    InputHelperSearchText.setFillColor(Color(255, 255, 255, 255));
                    InputHelperSearchText.setPosition(500, 767);

                
                    SearchText.setFillColor(Color(255, 255, 255));

                    AvailableInputSearchBar = true;

                    DOUBLE_input = 0;

                    AvailableInputFromHours = false;
                    AvailableInputFromMinutes = false;
                    AvailableInputFromSeconds = false;

                    AvailableInputToHours = false;
                    AvailableInputToMinutes = false;
                    AvailableInputToSeconds = false;

                    FromHoursText.setFillColor(Color(118, 134, 167));
                    FromMinutesText.setFillColor(Color(118, 134, 167));
                    FromSecondsText.setFillColor(Color(118, 134, 167));

                    ToHoursText.setFillColor(Color(118, 134, 167));
                    ToMinutesText.setFillColor(Color(118, 134, 167));
                    ToSecondsText.setFillColor(Color(118, 134, 167));

                    InputHelperText.setFillColor(Color(0, 0, 0, 0));

                }

  
        
      
        //кнопка type
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (button1.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    button1.setFillColor(Color(0, 0, 0, 70));
                }
        if (event.type == Event::MouseButtonReleased)
            if (event.key.code == Mouse::Left)
            {
                button1.setFillColor(Color(0, 0, 0, 0));
            }

        //кнопка massage
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (button2.getGlobalBounds().contains(MousePosition.x, MousePosition.y)) 
                {
                    button2.setFillColor(Color(0, 0, 0, 70));
                }
        if (event.type == Event::MouseButtonReleased)
            if (event.key.code == Mouse::Left)
            {
                button2.setFillColor(Color(0, 0, 0, 0));
            }

        //скролин type
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (ScrollType.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    dXscrollType = MousePosition.x - ScrollType.getPosition().x;
                    ScrollTypePressed = true;
                }
        if (event.type == Event::MouseButtonReleased)
                if (event.key.code == Mouse::Left)
                {
                    ScrollType.setFillColor(Color::Black);
                    ScrollTypePressed = false;
                }
        

       //скроллинг message
        if (event.type == Event::MouseButtonPressed)
            if (event.key.code == Mouse::Left)
                if (ScrollMessage.getGlobalBounds().contains(MousePosition.x, MousePosition.y))
                {
                    dXscrollMessage = MousePosition.x - ScrollMessage.getPosition().x;
                    ScrollMessagePressed = true;
                }
       if (event.type == Event::MouseButtonReleased)
                if (event.key.code == Mouse::Left)
                {
                    ScrollMessage.setFillColor(Color::Black);
                    ScrollMessagePressed = false;
                }
        


        //скрол type начало
        if (ScrollTypePressed == true)
        {
            ScrollType.setFillColor(Color::Green);
            if (ScrollType.getPosition().x >= 371 && ScrollType.getPosition().x <= 610 && SetPositionScrollType == false)
            {
                ScrollType.setPosition(MousePosition.x - dXscrollType, 718);  //скролл
            }
            else if (ScrollType.getPosition().x < 371 && SetPositionScrollType == false)
            {
                ScrollType.setPosition(371, 718);
                OlMousePositionScrollType = MousePosition.x;
                SetPositionScrollType = true;
            }
            else if (ScrollType.getPosition().x > 610 && SetPositionScrollType == false)
            {
                ScrollType.setPosition(610, 718);
                OlMousePositionScrollType = MousePosition.x;
                SetPositionScrollType = true;
            }


            if (SetPositionScrollType == true)
            {
                if (OlMousePositionScrollType < MousePosition.x && ScrollType.getPosition().x == 371)
                {
                    SetPositionScrollType = false;
                }
                else if (OlMousePositionScrollType > MousePosition.x&& ScrollType.getPosition().x == 610)
                {
                    SetPositionScrollType = false;
                }
            }
        }
        //скрол type конец


        //скрол message начало
        if (ScrollMessagePressed == true)
        {
            ScrollMessage.setFillColor(Color::Green);                                  //1215
            if (ScrollMessage.getPosition().x >= 680 && ScrollMessage.getPosition().x <= 1095 && SetPositionScrollMessage == false)
            {
                ScrollMessage.setPosition(MousePosition.x - dXscrollMessage, 718); //скролл
            }
            else if (ScrollMessage.getPosition().x < 680 && SetPositionScrollMessage == false)
            {
                ScrollMessage.setPosition(680, 718);
                OlMousePositionScrollMessage = MousePosition.x;
                SetPositionScrollMessage = true;
            }
            else if (ScrollMessage.getPosition().x > 1095 && SetPositionScrollMessage == false)
            {
                ScrollMessage.setPosition(1095, 718);
                OlMousePositionScrollMessage = MousePosition.x;
                SetPositionScrollMessage = true;
            }


            if (SetPositionScrollMessage == true)
            {
                if (OlMousePositionScrollMessage < MousePosition.x && ScrollMessage.getPosition().x == 680)
                {
                    SetPositionScrollMessage = false;
                }
                else if (OlMousePositionScrollMessage > MousePosition.x&& ScrollMessage.getPosition().x == 1095)
                {
                    SetPositionScrollMessage = false;
                }
            }
        }
        //скрол message конец



        window.clear();
        window.draw(backgSprite);
        window.draw(button1);
        window.draw(button2);
        window.draw(ScrollType);
        window.draw(ScrollMessage);
        window.draw(FromHoursText);
        window.draw(FromMinutesText);
        window.draw(FromSecondsText);
        window.draw(ToHoursText);
        window.draw(ToMinutesText);
        window.draw(ToSecondsText);
        window.draw(InputHelperText);
        window.draw(SearchText);
        window.draw(InputHelperSearchText);
        window.display();
    }
        return EXIT_SUCCESS;
    
}